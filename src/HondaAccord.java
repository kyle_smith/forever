
public class HondaAccord extends ACar{

	public HondaAccord(int inYear, int inMileage) {
		super("Honda", "Accord", 2016);
		setMileage(inMileage);
	}

}
